import { Module } from '@nestjs/common';
import {DynamoGateway} from './gateway/dynamo.gateway';
import { MutationModule } from './mutation/mutation.module';

@Module({
  imports: [MutationModule],
  controllers: [],
  providers: [DynamoGateway],
})
export class AppModule {}
