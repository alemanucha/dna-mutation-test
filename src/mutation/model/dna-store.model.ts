export class DnaStoreModel {
  id: string;
  created: number;
  dnaChain: object;
  hasMutation: boolean;
}