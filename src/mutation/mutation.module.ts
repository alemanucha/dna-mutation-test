import { Module } from '@nestjs/common';
import {DynamoGateway} from 'src/gateway/dynamo.gateway';
import { MutationController } from './mutation.controller';
import { MutationService } from './mutation.service';

@Module({
  controllers: [MutationController],
  providers: [
    MutationService,
    DynamoGateway
  ],
  exports: [DynamoGateway]
})
export class MutationModule {}
