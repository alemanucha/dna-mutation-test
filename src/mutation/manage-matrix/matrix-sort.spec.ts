import {MatrixSort} from "./matrix-sort";

describe("string manage of matrix", () => {
  const originalMatrix: string[] = [
    "ATGCGA",
    "CAGTGC",
    "TTATGT",
    "AGAAGG",
    "CCCCTA",
    "TCACTG"
  ];

  test("pivot matrix", () => {
    const pivotMatrix: string[] = [
      "ACTACT",
      "TATGCC",
      "GGAACA",
      "CTTACC",
      "GGGGTT",
      "ACTGAG"
    ];
    const resultPivotMatrix: string[] = MatrixSort.pivot(originalMatrix);
    expect(resultPivotMatrix).toEqual(pivotMatrix);
  });

  test("diagonalPivot matrix", () => {
    const pivotMatrix: string[] = [
      "AAAATG",
      "AAAATG",
      "TGTGA",
      "CTACT",
      "GTGG",
      "TGCC",
      "CGT",
      "ACA",
      "GC",
      "CC",
      "A",
      "T",
    ];
    const resultPivotMatrix: string[] = MatrixSort.diagonalPivot(originalMatrix);
    expect(resultPivotMatrix).toEqual(pivotMatrix);
  });

  test("inverseDiagonalPivot matrix", () => {
    const pivotMatrix: string[] = [
      'AGTACT',
      'AGTACT',
      'GTAGC',
      'CGACC',
      'CGTA',
      'TGCA',
      'GAT',
      'GTC',
      'TC',
      'AT',
      'A',
      'G'
    ];
    const resultPivotMatrix: string[] = MatrixSort.diagonalInversePivot(originalMatrix);
    expect(resultPivotMatrix).toEqual(pivotMatrix);
  });
})