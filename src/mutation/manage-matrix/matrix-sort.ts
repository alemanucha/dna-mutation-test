export class MatrixSort {

  public static pivot(grid: string[]): string[] {
    const length: number = grid[0].length;
    let newGrid: string[] = [];
    for(let col: number = 0; col < length; col++) {
      let newstring = "";
      for(let row: number = 0; row < grid.length; row++) {
        newstring = newstring + grid[row][col];
      }
      newGrid.push(newstring);
    }
    return newGrid;
  }

  public static diagonalPivot(grid: string[]): string[] {
    let newGrid: string[] = [];
    let N: number = grid.length;
    for(let i: number = 0; i < N; i++) {
      let row = 0;
      let col = i;

      let text1: string = "";
      let text2: string = "";

      while(col < N && row < N) {
        text1 += grid[row][col];
        text2 += grid[col][row];
        row++;
        col++;
      }
      newGrid.push(text1);
      newGrid.push(text2);
    }
    return newGrid;
  }

  public static diagonalInversePivot (grid: string[]): string[] {
    const reverseGrid: string[] = grid.map(currentLine => currentLine.split("").reverse().join(""));
    return MatrixSort.diagonalPivot(reverseGrid);
  }
}