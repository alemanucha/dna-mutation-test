import {MatrixSort} from "./matrix-sort"

export const DirectionChain = {
  'VERTICAL': MatrixSort.pivot,
  'DIAGONAL': MatrixSort.diagonalPivot,
  'DIAGONAL_INVERSE': MatrixSort.diagonalInversePivot
}