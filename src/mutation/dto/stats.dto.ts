export class RatioDto {
  count_mutations: number;
  count_no_mutations: number;
  ratio: number;
}