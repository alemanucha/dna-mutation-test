import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {DynamoGateway} from '../gateway/dynamo.gateway';
import {DnaStoreModel} from './model/dna-store.model';
import {ValidateCharChain} from './validate-char-chains/validate-char-chain';
import { v4 } from "uuid";
import {TablesEnum} from './enum/tables.enum';
import {RatioDto} from './dto/stats.dto';

@Injectable()
export class MutationService {

  private readonly dynamoGateway: DynamoGateway = new DynamoGateway();
  private readonly validateCharChain = new ValidateCharChain();

  public async hasMutation(dnaGrid: string[]) {
    const result = this.validateCharChain.hasMutation(dnaGrid);
    const dnaStore: DnaStoreModel = {
      id: v4(),
      created: Date.now(),
      dnaChain: dnaGrid,
      hasMutation: result
    };
    const dynamoResult = await this.dynamoGateway.put(dnaStore, TablesEnum.DNA_MUTATION);
    if(!result) {
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
  }

  public async getStats(): Promise<RatioDto> {
    const count_mutations = await this.getMutations();
    const count_no_mutations = await this.getNoMutations();
    const ratio = this.calculateRatio(count_mutations, count_no_mutations);
    return {
      count_mutations,
      count_no_mutations,
      ratio
    };
  }

  private async getMutations(): Promise<number> {
    const stats = await this.dynamoGateway.get({
      TableName: TablesEnum.DNA_MUTATION,
      FilterExpression: "#hasMutation = :hasMutationValue",
      ExpressionAttributeNames: {
          "#hasMutation":"hasMutation"
      },
      ExpressionAttributeValues: {
          ":hasMutationValue":true
      },
      Select: "COUNT"
    });
    return stats.Count;
  }

  private async getNoMutations(): Promise<number> {
    const stats = await this.dynamoGateway.get({
      TableName: TablesEnum.DNA_MUTATION,
      FilterExpression: "#hasMutation = :hasMutationValue",
      ExpressionAttributeNames: {
          "#hasMutation":"hasMutation"
      },
      ExpressionAttributeValues: {
          ":hasMutationValue":false
      },
      Select: "COUNT"
    });
    return stats.Count;
  }

  private calculateRatio(withMutation: number, noMutation: number): number {
    return Number((withMutation / noMutation).toFixed(2));
  }
}
