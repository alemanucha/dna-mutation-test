import {ValidateCharChain} from "./validate-char-chain"

describe("validate mutations or dna chain", () => {
  const validateCharChain: ValidateCharChain = new ValidateCharChain();

  test("instancia de ValidateCharChain", () => {
    expect(validateCharChain).toBeInstanceOf(ValidateCharChain);
  });
  test("with mutation", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTGC",
      "TTATGT",
      "AGAAGG",
      "CCCCTA",
      "TCACTG"
    ];
    const resultado = validateCharChain.hasMutation(dna);
    expect(resultado).toBe(true);
  });
  test("with diagonal mutation", () => {

    const dna: string[] = [
      "ATGCGA",
      "CAGGGC",
      "TTGTTT",
      "AGACGG",
      "GCGTCA",
      "TCACTG"
    ];
    const resultado = validateCharChain.hasMutation(dna);
    expect(resultado).toBe(true);
  });
  test("with diagonal inverse mutation", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTGC",
      "TTATTT",
      "AGAAGG",
      "GCGTCA",
      "TCACTG"
    ];
    const resultado = validateCharChain.hasMutation(dna);
    expect(resultado).toBe(true);
  });
  test("with no mutation", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTGC",
      "TTATTT",
      "AGACGG",
      "GCGTCA",
      "TCACTG"
    ];
    const resultado = validateCharChain.hasMutation(dna);
    expect(resultado).toBe(false);
  });
  test("mutation with invalid length", () => {
    const dna: string[] = [
      "ATGCG",
      "CAGTGC",
      "TTATTT",
      "AGACGGG",
      "GCGTCA",
      "TCACTG"
    ];
    const execution = () => {
      validateCharChain.hasMutation(dna);
    }
    expect(execution).toThrow(Error);
  });
  test("mutation with invalid characters", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTGY",
      "TTATTT",
      "AGACGG",
      "GCGTCA",
      "TCACTG"
    ];
    const execution = () => {
      validateCharChain.hasMutation(dna);
    }
    expect(execution).toThrow(Error);
  });
  test("correct length", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTGC",
      "TTATTT",
      "AGACGG",
      "GCGTCA",
      "TCACTG"
    ];
    const resultado = validateCharChain.validateLength(dna);
    expect(resultado).toBe(true);
  });
  test("different length", () => {
    const dna: string[] = [
      "ATGCG",
      "CAGTGC",
      "TTATT",
      "AGACGG",
      "GCGTCAG",
      "TCACTG"
    ];
    const resultado = validateCharChain.validateLength(dna);
    expect(resultado).toBe(false);
  });
  test("valid characters", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTGC",
      "TTATTT",
      "AGACTG",
      "GCGTCA",
      "TCACTG"
    ];
    const resultado = validateCharChain.validateCharacters(dna);
    expect(resultado).toBe(true);
  });
  test("invalid characters", () => {
    const dna: string[] = [
      "ATGCGA",
      "CAGTEC",
      "TTATTT",
      "AGACTG",
      "GCGTCA",
      "TCACTG"
    ];
    const resultado = validateCharChain.validateCharacters(dna);
    expect(resultado).toBe(false);
  });
})