import {doesNotMatch} from "assert";
import {DirectionChain} from "../manage-matrix/direction-chain";
import {MutationError} from "./mutation-error";

export class ValidateCharChain {
  private readonly mutationRegex = /[A]{4}|[T]{4}|[C]{4}|[G]{4}/g;

  public hasMutation(grid: string[]): boolean {
    if(!this.validateCharacters(grid) || !this.validateLength(grid)) {
      throw new Error("Invalid dna");
    }
    try {
      this.searchCol(grid);
      Object.keys(DirectionChain).forEach(functionKey => {
        const newGrid: string[] = DirectionChain[functionKey](grid);
        this.searchCol(newGrid);
      });
      return false;
    } catch (error) {
      if(error instanceof MutationError) {
        return true;
      }
      throw error;
    }
  }

  public validateLength(grid: string[]) {
    const result: string[] = grid.filter( dnaLine => dnaLine.length > 6 || dnaLine.length < 6);
    return !result.length;
  }

  public validateCharacters(grid: string[]) {
    const validBaseRegex = /[^A|T|C|G]/gm;
    const result: string[] = grid.filter( dnaLine => validBaseRegex.test(dnaLine));
    return !result.length;
  }

  public searchCol(grid: string[]) {
    for(let col: number = 0; col < grid.length; col++) {
      if(grid[col].length < 4) {
        continue;
      }
      const result = this.mutationRegex.test(grid[col]);
      if(result) {
        new Error();
        throw new MutationError("DNA has mutation")
      }
    }
  }
}