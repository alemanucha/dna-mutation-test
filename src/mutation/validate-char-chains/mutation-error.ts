export class MutationError<E = any, T = any> extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, MutationError.prototype);
  }
}