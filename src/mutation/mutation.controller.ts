import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import {MutationCheckDto} from './dto/mutation-check.dto';
import {MutationService} from './mutation.service';

@Controller('mutation')
export class MutationController {
  constructor(
    private readonly service: MutationService
  ) {}

  @Post()
  @HttpCode(200)
  async hasMutation(
    @Body() mutationCheck: MutationCheckDto
  ) {
    return this.service.hasMutation(mutationCheck.dna);
  }

  @Get("/stats")
  getStatus() {
    return this.service.getStats();
  }
}
