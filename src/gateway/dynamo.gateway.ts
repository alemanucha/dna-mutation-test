import {
  DocumentClient
} from "aws-sdk/clients/dynamodb";
import {DnaStoreModel} from "src/mutation/model/dna-store.model";

export class DynamoGateway {
  private readonly _client: DocumentClient = new DocumentClient({region: "us-east-1"});

  public put(data: DnaStoreModel, table: string) {
    const params: DocumentClient.PutItemInput = {
      Item: data,
      TableName: table
    };
    return this._client.put(params).promise();
  }

  public get(searchQuery: DocumentClient.ScanInput) {
    return this._client.scan(searchQuery).promise();
  }
}