## Description

Api to test dna chains and response if has mutations, also reports the total with mutations and de ratio

The app has CI with gitlab pipelines, deploys on lambda function and store the data on DynamoDB

## Installation

```bash
$ npm install
$ npm install -g serverless
```

## Running the app

```bash
# development

You need aws credentias to connect with DynamoDB

$ npm run build
$ sls offline start
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```